# -*- coding: utf-8 -*-
"""
Created on Thu Nov 26 10:59:29 2020

Module qui permet de faire des manipulations d'images pour déterminer unen valeur de luminosité

@author: Enzo Magal & Brice Berthommé
"""

import numpy as np
import cv2
import matplotlib.pyplot as plt
#import mahotas as mh
import contour as ct


def extraction(file):
    img = cv2.imread(file)
    return cv2.cvtColor(img, cv2.COLOR_BGR2RGB)


def trim(img,up, down, left, right):
    h,w,_ = img.shape
    h=h-down
    w=w-right
    img=img[up:h,left:w]
    return img,h,w


def affichage_debug(file):

    imgcv = extraction(file)
    imgcv,h,w = trim(imgcv,0,60,0,0)
    imgcv = cv2.medianBlur(imgcv,21)

    sobelx = cv2.Sobel(imgcv,cv2.CV_64F,1,0,ksize=5)
    sobely = cv2.Sobel(imgcv,cv2.CV_64F,0,1,ksize=5)
    abs_grad_x = cv2.convertScaleAbs(sobelx)
    abs_grad_y = cv2.convertScaleAbs(sobely)
    modgrad = cv2.addWeighted(abs_grad_x, 0.5, abs_grad_y, 0.5, 0)

    gray = cv2.cvtColor(modgrad, cv2.COLOR_RGB2GRAY)
    Lcnt = ct.contourMax(imgcv,10,0.01)
    hull = cv2.convexHull(Lcnt[0])
    cv2.drawContours(imgcv, Lcnt, -1, (0, 255, 0), 2)
    plt.imshow(imgcv)
    plt.show()
    return len(Lcnt)