#! /usr/bin/env python3
# coding: utf-8

"""
Created on Thu Dec 17 10:59:29 2020

Script à éxecuter pour obtenir les courbes représentant les différents paramètres

@author: Brice Berthommé & Enzo Magal
"""


### Importation des différents modules de fonction ###

import os
import numpy as np
import matplotlib.pyplot as plt
import luminosity as lum
import lvlgray as lg

def main():

    dossier = os.listdir("../2020-07-06-PS")    #fichier et la liste des fichier contenu dans le répertoire 2020-07-06-PS

    ### Courbe représentant la moyenne des niveaux de gris ###

    X = np.zeros(len(dossier))          #tableau de zeros de taille du nombre d'image que l'on va traiter
    Y = np.zeros(len(dossier))
    i = 0                               #curseur de fichier

    for fichier in dossier:                                   #on parcour tout les fichier de notre dossier
        X[i] = i                                              #les X sont les entiers corespondant à une image de la série
        Y[i] = lg.lvlgray("../2020-07-06-PS/image-"+ str(i+1)+".jpg")          #les Y sont les valeurs de moyenne calculées
        i += 1

    Y = Y/max(Y)            #normalisation de nos valeurs

    plt.plot(X, Y, "g:")      #on trace la courbe des Y en focntion des X

    ### Courbe représentant la luminosité ###

    Y2 = np.zeros(len(dossier))
    i = 0                               #curseur de fichier

    seuil = 130             #valeur de seuillage choisi suite à plusieurs essaies

    for fichier in dossier:                                   #on parcour tout les fichier de notre dossier                                             #les X sont les entiers corespondant à une image de la série
        Y2[i] = lum.luminosity("../2020-07-06-PS/image-"+ str(i+1)+".jpg", seuil)          #les Y sont les valeurs de moyenne calculées
        i += 1

    Y2 = Y2/max(Y2)            #normalisation de nos valeurs

    plt.plot(X, Y2, "r:")      #on trace la courbe des Y en focntion des X

    plt.show()          #on affiche notre courbe

if __name__ == "__main__":
    main()