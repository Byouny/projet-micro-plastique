# -*- coding: utf-8 -*-
"""
Created on Thu Nov 26 10:59:29 2020

Module permettant de determiner la valeur de niveau de gris moyen d'une image

@author: Enzo Magal & Brice Berthommé
"""

import numpy as np
import mahotas as mh

def lvlgray(nom_fichier):
    ### Calcule une valeur de luminosité ###
    image = mh.imread(nom_fichier)                      #ouverture de mon image
    image = image[:, :, 0]                                  #transforme l'image en niveau de gris
    T = np.array(image)                                 #transforme mon image en tableau de valeur
    avg = np.average(T)                                 #calcule la moyenne des valeurs de mon tableau
    return avg
