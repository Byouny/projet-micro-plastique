# -*- coding: utf-8 -*-
"""
Created on Thu Nov 26 10:59:29 2020

Module qui permet de comparer des courbes entre elles

@author: Enzo Magal & Brice Berthommé
"""

import numpy as np
from scipy.spatial import distance
import matplotlib.pyplot as plt

def compare(y1, y2):
    res = 0
    for i in range(np.size(y1)):
        res = res + abs(y1[i]-y2[i])
    res = res/np.size(y1)
    return res


y1 = np.array([2.0, 0.0, 0.0, 1.0])
y2 = np.array([2.0, 1.0, 2.0, 0.0])
x = np.array([0.0, 1.0, 2.0, 3.0])

plt.plot(x, y1, y2)

plt.show()

print(compare(y1, y2))


def particles_compare(val1, val2):
    res = [0, 0, 0, 0]
    res[0] = compare(val1[0], val2[0])
    B_comp = compare(val1[1][0], val2[1][0])
    R_comp = compare(val1[1][1], val2[1][1])
    G_comp = compare(val1[1][2], val2[1][2])
    color_comp = (B_comp + R_comp + G_comp)/3
    res[1] = color_comp
    res[2] = compare(val1[2], val2[2])
    res[3] = compare(val1[3], val2[3])
    return res


def ponderation(carac):
    pond = 0
    s = 0
    for i in range(len(carac)-1):
        for j in range(i+1,len(carac)):
            pond += distance.euclidean(carac[i], carac[j])
    pond = pond/((len(carac)-1)*len(carac)*len(carac[0])/2)
    return pond






