# Projet Micro-Plastique

Création : 20 octobre 2020

Ceci est un projet de ZZ2 de *Brice Berthommé* et *Enzo Magal*. Nous sommes étudiant en deuxième année à l'ISIMA. Nous travaillont actuellement sur ce projet encadré par des chercheurs du laboratoire ICCF : Julien Devemy et Jean-Michel Andanson. 
Ce projet porte sur l'observation de changement d'état de particules de microplastiques grâce à des algorithme de reconnaissance d'image.

## But du projet

Ce projet a plusieurs buts :
- Pour une série, afficher différents graphs d’évolution de caractéristiques des images selon le
temps (luminosité, taille de particules, ...)
- Pour chaque graph, essayer de fitter une fonction définie à l’avance pour détecter une
température représentative d’une transformation
- Trouver des températures représentatives de chaque type de polymère
- Caractériser une nouvelle série d’images selon les températures représentatives d’une
transformation

## Utilisation

Il faut pour utiliser notre code éxecuter le fichier main.py (qui contiendra la fonction main). Ce fichier fait appelle à tout les autres modules et permet d'obtnir les grpahiques. 

Pour l'instant le code ne fonctionne pas tout seul car les chemins d'accés au fichier ne correspondent pas mais ça ne va pas tarder à être modifié. 

## Modules

### lvlgray :

But :
Ce module permet de déterminer une valeur de niveau de gris moyen de nos images. Il s'agit plus d'une première approche que d'une véritable caractéristique d'observation.

Fonctions :
- lvlgray : calcule la valeur moyenne de niveau de gris d'une image

### luminosity :

But :
Ce module permet de déterminer une valeur, qui est censé représenter la luminosité d'une image. C'est une approche à peine plus poussé que simplement regarder le niveau de gris de l'image.

Fonctions :
- seuillage : seuille une image
- luminosity : calcule une valeur de luminosité


PS : le fichier script.py sert uniquement de base de travail il n'est pas nécessaire dans l'utilisation du main et serra enlevé à la finalisation du projet