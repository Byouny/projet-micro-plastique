# -*- coding: utf-8 -*-
"""
Created on Thu Nov 26 10:59:29 2020

Module qui permet de faire des manipulations d'images pour déterminer unen valeur de luminosité

@author: Enzo Magal & Brice Berthommé
"""

import numpy as np
import cv2
import matplotlib.pyplot as plt
#import mahotas as mh
from scipy.spatial import distance
"""from pylab import imshow, show"""


def cntSelection(cnts,minArea):
    Lcnt=[]
    Lcnt2=[]
    for i in range(len(cnts)):
        if cv2.contourArea(cnts[i]) > minArea:
            Lcnt.append(cnts[i])
            
    for i in range(len(Lcnt)):
        ok=1
        for j in range(len(Lcnt)):
            if i!=j:
                c=Lcnt[j]
                extLeft = c[c[:, :, 0].argmin()][0][0]
                extRight = c[c[:, :, 0].argmax()][0][0]
                extTop = c[c[:, :, 1].argmin()][0][1]
                extBot = c[c[:, :, 1].argmax()][0][1]
                point = Lcnt[i][0][0]
                if point[0]<=extRight and point[0]>=extLeft and point[1]>=extTop and point[1]<=extBot:
                    ok=0
        if ok:
            Lcnt2.append(Lcnt[i])
    return Lcnt2


def sobel(img):
    sobelx = cv2.Sobel(img,cv2.CV_32F,1,0,ksize=5)
    sobely = cv2.Sobel(img,cv2.CV_32F,0,1,ksize=5)
    abs_grad_x = cv2.convertScaleAbs(sobelx)
    abs_grad_y = cv2.convertScaleAbs(sobely)
    modgrad = cv2.addWeighted(abs_grad_x, 0.5, abs_grad_y, 0.5, 0)
    phase=cv2.phase(sobelx,sobely,True)
    #phase=np.arctan2(sobelx,sobely)
    return modgrad,phase

def contourMax(image,limit,alpha):
    img=image.copy()
    img = cv2.medianBlur(img,21)
    modgrad,_ = sobel(img)
    gray = cv2.cvtColor(modgrad, cv2.COLOR_RGB2GRAY)

    thresh = cv2.threshold(gray, limit, 255, cv2.THRESH_BINARY)[1]
    thresh = cv2.erode(thresh, None, iterations=2)
    thresh = cv2.dilate(thresh, None, iterations=2)

    cnts,_ = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    h,w,_=img.shape

    minArea = h*w*alpha

    """
    extLeft = tuple(c[c[:, :, 0].argmin()][0])
    extRight = tuple(c[c[:, :, 0].argmax()][0])
    extTop = tuple(c[c[:, :, 1].argmin()][0])
    extBot = tuple(c[c[:, :, 1].argmax()][0])

    cv2.drawContours(imagecv2, [c], -1, (0, 255, 255), 2)
    cv2.circle(imagecv2, extLeft, 8, (0, 0, 255), -1)
    cv2.circle(imagecv2, extRight, 8, (0, 255, 0), -1)
    cv2.circle(imagecv2, extTop, 8, (255, 0, 0), -1)
    cv2.circle(imagecv2, extBot, 8, (255, 255, 0), -1)
    """

    return cntSelection(cnts,minArea)

def make_mask(cnt,img):
    #mask
    h,w,_ =img.shape
    mask = np.zeros((h,w),np.uint8)
    cv2.drawContours(mask,[cnt],0,255,-1)
    return mask

def infoCell(cnt, img):

    h,w,_ = img.shape
    mask=make_mask(cnt,img)

    #size
    area = cv2.contourArea(cnt)

    #Solidity
    hull = cv2.convexHull(cnt)
    hull_area = cv2.contourArea(hull)
    solidity = float(area)/hull_area

    #Color
    color = cv2.mean(img,mask = mask)
    return area,color,solidity

def phase_histo(cnt,img):
    gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
    _,phase = sobel(gray)
    mask=make_mask(cnt,img)
    hist = cv2.calcHist([np.multiply(phase,mask)],[0], None, [360], [0,360])
    return hist


def phase_variation(newHist, hist):
    #histogram phase variation

    #V = np.cov(np.array([hist, newHist]).T)
    #IV = np.linalg.inv(V)
    #phase_hist_variation = distance.mahalanobis(hist, newHist, IV)
    phase_hist_variation = distance.euclidean(hist, newHist)
    return phase_hist_variation


def center(cnt):
    """
    M = cv2.moments(cnt)
    cx = int(M['m10']/M['m00'])
    cy = int(M['m01']/M['m00'])
    """
    extLeft = cnt[:, :, 0].argmin()
    extRight = cnt[:, :, 0].argmax()
    extTop = cnt[:, :, 1].argmin()
    extBot = cnt[:, :, 1].argmax()
    return (int((extLeft+extRight)/2),int((extTop+extBot)/2))


def distCenter(p1,p2):
    return np.sqrt(np.power((p1[0]-p2[0]),2)+np.power((p1[1]-p2[1]),2))


def findCoordinate(Lcenter,center):
    return np.argmin([distCenter(Lcenter[k],center) for k in range(len(Lcenter))])

def isConvex(cnt):
    return cv2.contourArea(cv2.convexHull(cnt))>10*cv2.contourArea(cnt)


def gradient(nom_fichier):
    img = cv2.imread(nom_fichier)
    sobelx = cv2.Sobel(img,cv2.CV_64F,1,0,ksize=5)
    sobely = cv2.Sobel(img,cv2.CV_64F,0,1,ksize=5)
    abs_grad_x = cv2.convertScaleAbs(sobelx)
    abs_grad_y = cv2.convertScaleAbs(sobely)
    modgrad = cv2.addWeighted(abs_grad_x, 0.5, abs_grad_y, 0.5, 0)
    gray = cv2.cvtColor(modgrad, cv2.COLOR_BGR2GRAY)

    plt.subplot(3,2,1),plt.imshow(img,cmap = 'gray')
    plt.title('Original'), plt.xticks([]), plt.yticks([])

    plt.subplot(3,2,2),plt.imshow(modgrad,cmap = 'gray')
    plt.title('Module gradient'), plt.xticks([]), plt.yticks([])

    plt.subplot(3,2,3),plt.imshow(sobelx,cmap = 'gray')
    plt.title('Sobel X'), plt.xticks([]), plt.yticks([])

    plt.subplot(3,2,4),plt.imshow(sobely,cmap = 'gray')
    plt.title('Sobel Y'), plt.xticks([]), plt.yticks([])

    plt.subplot(3,2,5),plt.imshow(gray,cmap = 'gray')
    plt.title('Sobel Y'), plt.xticks([]), plt.yticks([])

    plt.show()
























