import numpy as np
import mahotas as mh
from pylab import gray, imshow, show
import matplotlib.pyplot as plt
import scipy as scp
import PIL
from PIL import ImageFilter
from PIL import ImageDraw
import cv2
import os


def afficher_imag(nom_fichier):

    I = PIL.Image.open(nom_fichier)
    (l,h) = I.size        #taille image
    Mode = I.mode         #mode de couleur (RGB, Luminance, CYMK, ...)
    Format = I.format     #PNG, JPG, BMP ...

    Ig=I.convert('L')   #Img en niveau de gris


    Ig2 = Ig.filter(ImageFilter.GaussianBlur(30))
    #Ig2.show()
    Q=quadri(h,l,100)
    T=np.array(Ig2)
    localmin(Q,T,500)
    affmin(Q,Ig2)

    """
    T=np.array(I)
    for i in range(26):
        Tg=binimg(np.array(Ig),i*10,(i+1)*10)
        Iga = PIL.Image.fromarray(Tg)
        Iga.save('img_alpha_' + str(i*10) +'-'+str((i+1)*10)+'.tiff')
"""


def quadri(h,l,alpha):
    return[(alpha*i,alpha*j) for i in range(1,h//alpha) for j in range(1,l//alpha)]


def localmin(Q,T,maxit):
    z=0
    alpha=20
    (max_i,max_j)=T.shape
    while z<maxit:
        for n in range(len(Q)):
            (i,j)=Q[n]
            mi,mj,=i,j
            mz=T[mi][mj]
            inf_i,sup_i,inf_j,sup_j=0,0,0,0
            if i<alpha:
                inf_i=0
            else:
                inf_i=i-alpha
            if i>=max_i-alpha:
                sup_i=max_i
            else:
                sup_i=i+alpha
            if j<alpha:
                inf_j=0
            else:
                inf_j=j-alpha
            if j>=max_j-alpha:
                sup_j=max_j
            else:
                sup_j=j+alpha

            for di in range(inf_i,sup_i):
                for dj in range(inf_j,sup_j):
                    m=T[di][dj]
                    if m<mz:
                        mz,mi,mj=m,di,dj
            Q[n]=(mi,mj)
        z+=1

def affmin(Q,Ig2):
    (l,h) = Ig2.size
    imgmin = ImageDraw.Draw(Ig2)
    for L in Q:
        if L[0]>20 and L[1]>20 and L[0]<h-20 and L[1]<l-20:
            imgmin.ellipse([(L[0]-5,L[1]-5),(L[0]+5,L[1]+5)],fill=255)
    Ig2.show()



def binimg(T,alpha,beta=0):
    h,l=T.shape
    M = np.zeros((h,l))
    if beta==0 :
        for i in range(h):
            for j in range(l):
                if alpha < T[i][j] :
                    M[i][j] = 255
    else:
        for i in range(h):
            for j in range(l):
                if alpha < T[i][j] < beta :
                    M[i][j] = 255
    return M






def afficher_courbe(nom_dossier,n):
    #CV=[]
    CX=[]
    X=[i for i in range(1,n)]
    for i in range(1,n):
        I1 = PIL.Image.open(nom_dossier+"/image-"+str(i)+".jpg")
        I2 = PIL.Image.open(nom_dossier+"/image-"+str(i+1)+".jpg")
        (l,h) = I1.size        #taille image
        Mode = I1.mode         #mode de couleur (RGB, Luminance, CYMK, ...)
        Format = I1.format     #PNG, JPG, BMP ...
        Ig1=I1.convert('L')   #Img en niveau de gris
        Ig2=I2.convert('L')
        T1=np.array(Ig1)
        T2=np.array(Ig2)
        TX=np.average(T1)
        #TV=T1-T2
       # CV.append(np.average(TV))
        CX.append(TX)
    plt.clf()
    plt.plot(X,CX,'bo')
    #plt.plot(X,CV,'go')
    plt.show()




def test_mh():
    #"Microplastique-Microscopie/2020-07-06-PS/image-1.jpg"
    #f=cv2.imread("Microplastique-Microscopie/2020-07-06-PS/image-1.jpg")
    f=mh.imread("Microplastique-Microscopie/2020-07-06-PS/image-1.jpg")
    f=f[:,:,0]
    #(l,h) = f.shape
    #f =mh.gaussian_filter(f,10)
    #Cir=np.zeros((h,l))
    #Cir2=cv2.circle(Cir,(int(h/2),int(l/2)),int(l/2),1,int(l/2))
    #cv2.imshow("test",Cir2)
    f = (f>130)
    imshow(f)       # charge l'image à afficher
    show()          # affiche l'image


def afficher_courbe2(nom_dossier,n):
    CX=[]
    X=[i for i in range(1,n)]
    for i in range(1,n):
        f=mh.imread(nom_dossier+"/image-"+str(i)+".jpg")
        f=f[:,:,0]
        f = (f>130)
        T=np.array(f)
        CX.append(np.average(T))
    plt.clf()
    plt.plot(X,CX,'bo')
    plt.show()


def propre(folder_path):
    list_file = os.listdir(folder_path) #Microplastique-Microscopie/2020-07-06-PS/
    n = len(list_file)
    F = mh.imread(folder_path + list_file[0])


    F=F.max(2)
    (h,w) = F.shape

    #fillgap
    Fbin = (F < F.mean())
    #gray()

    Cir=np.ones((h,w))
    Cir2=cv2.circle(Cir,(int(w/2),int(h/2)),int(w),0,int(w*1.1))
    Fbin = Fbin*Cir2
    Fbin=Fbin[:h-60,:]
    """
    Fmask = np.zeros((h-60,w))
    mh.polygon.fill_polygon(mh.polygon.convexhull(Fbin),Fmask,color=1)
    imshow(Fbin)
    show()
    """
    image = cv2.imread(folder_path + list_file[0])
    gray = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)
    #gray = cv2.medianBlur(gray,5)
    #_, binary = cv2.threshold(gray, 80, 255, cv2.THRESH_BINARY_INV)
    binary = cv2.adaptiveThreshold(gray,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY_INV,11,2)
    contours, hierarchy = cv2.findContours(binary, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    image = cv2.drawContours(image, contours, -1, (0, 255, 0), 2)
    plt.imshow(image)
    plt.show()
