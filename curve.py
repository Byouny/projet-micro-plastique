# -*- coding: utf-8 -*-
"""
Created on Thu Nov 26 10:59:29 2020

Module qui permet de faire des manipulations d'images pour déterminer une valeur de luminosité

@author: Enzo Magal & Brice Berthommé
"""

import numpy as np
import matplotlib.pyplot as plt

def completion(a, b, Y):
    A = (Y[b]-Y[a])/(b-a)
    B = Y[a] - A*a
    for i in range(a,b):
        Y[i] = A*i + B


"""
x = np.array([0, 1, 2, 3])
y = np.array([2.0, 0.0, 0.0, 1.0])

plt.plot(x, y)

plt.show()

completion(1,3,y)

print(y)

plt.plot(x, y)

plt.show()
"""

def compl_curve(Y):
    i = 0
    while i != np.size(Y):
        if Y[i] == -1:
            a = i-1
            while Y[i] == -1:
                i += 1
            b = i
            completion(a, b, Y)
        i += 1
    return Y
      
x = np.array([0, 1, 2, 3, 4, 5, 6, 7, 8])
y = np.array([2.0, 0.0, 10.0, -1.0, -1.0, -1.0, -1.0, 4.0, 2.0])

plt.plot(x, y)

plt.show()

compl_curve(y)

print(y)

plt.plot(x, y)

plt.show()
