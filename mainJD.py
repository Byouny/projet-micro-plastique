#! /usr/bin/env python3
# coding: utf-8

import os
import numpy as np
import matplotlib.pyplot as plt
import cv2
#import luminosity as lum
#import lvlgray as lg
import contour as ct
import image_processing as proc
import compare as comp
import logging
import time
import curve as crv


DIRS = ("LDPE-30-150-30-400-2", "2021-01-14-PS-test4-30-150-30-400-nolamelle", "2021-01-14-LLDPE-test2-30-150-30-400-nolamelle", "2021-01-13-HDPE-test2-30-150-30-400", "2021-01-12-PVC-test1-30-150-30-400")
#start_time = time.time()

def main():

    logging.getLogger().setLevel(logging.INFO)

    plt.figure(figsize=(19, 12))

    L_pond = []

    for (i_dir, dir_) in enumerate(DIRS):
        #plt.subplot(1, 2, i_dir+1)

        files = os.listdir("../%s" % dir_)

        nb_files = len(files)

        # Search for imin and imax and check for holes
        indexes = [ int(fname.split("-")[1].split(".")[0]) for fname in files ]
        imin = min(indexes)
        imax = max(indexes)
        logging.info("Directory %s, Min:%d Max:%d", dir_, imin, imax)

        if (imax - imin) + 1 != nb_files:
            logging.warning("Directory %s: there are inconsistent in the image files", dir_)

        x = range(imin, imax + 1)

        # Compute

        # Image initialisation
        file = "../%s/image-1.jpg" % (dir_)
        imgcv = proc.extraction(file)

        # Trimming
        imgcv,h,w = proc.trim(imgcv,0,60,0,0)

        # Highest contours dectection
        Lcnt = ct.contourMax(imgcv,10,0.01)

        # N = Ininial particle number
        N=len(Lcnt)
        n=N

        #list of the initials centroîds
        Lcenter=[ct.center(Lcnt[k]) for k in range(N)]

        # Result array
        vals = [[] for k in range(N)]
        Lhist = [ct.phase_histo(Lcnt[k],imgcv) for k in range(N) ]
        # For each particle
        for k in range(N):

            # Collect data
            area,color,solidity = ct.infoCell(Lcnt[k], imgcv)
            vals[k].append((area,color[0],color[1],color[2],solidity,0))

        #calcul
        for i in x[1:50]:

            # Image nitialisation
            file = "../%s/image-%d.jpg" % (dir_, i)
            imgcv = proc.extraction(file)

            # Trimming
            imgcv,h,w = proc.trim(imgcv,0,60,0,0)
            # Highest contours dectection
            Lcnt = ct.contourMax(imgcv,10,0.01)

            # New particle number
            Newn=len(Lcnt)

            # If there is a change
            if Newn!=n:
                print("Changement dans les contours détecté en " + str(i))
                n=Newn

            # If there is at least one contour
            if n!=0:

                # New center list
                NewLcenter=[ct.center(Lcnt[k]) for k in range(n)]

                # For each intial particle
                for k in range(N):

                    # Indice of the nearest initial center detect to associate the contour and the particle
                    # if two particle touch each other, the restult wil be the same for the two of them
                    IdCoor=ct.findCoordinate(NewLcenter,Lcenter[k])

                    # If the contour is really not convex
                    if ct.isNotConvex(Lcnt[IdCoor]):

                        # We use convex Hull instead
                        area,color,solidity = ct.infoCell(cv2.convexHull(Lcnt[IdCoor]), imgcv)
                        newPhase = ct.phase_histo(cv2.convexHull(Lcnt[IdCoor]),imgcv)
                        phaseVar = ct.phase_variation(newPhase.flatten(),Lhist[IdCoor].flatten())
                        Lhist[IdCoor] = newPhase
                    else:
                        # Otherwise we use the contour
                        area,color,solidity = ct.infoCell(Lcnt[IdCoor], imgcv)
                        newPhase = ct.phase_histo(Lcnt[IdCoor],imgcv)
                        phaseVar = ct.phase_variation(newPhase.flatten(),Lhist[IdCoor].flatten())
                        Lhist[IdCoor] = newPhase
                        #newPhase.mean()
                    vals[k].append((area,color[0],color[1],color[2],solidity,phaseVar))
            else:
                print("Pas de contour détecté en " + str(i))
                for k in range(N):
                    vals[k].append((-1,-1,-1,-1,-1,-1))

            #lvl_gray = lg.lvlgray(image)
            #lumin = lum.luminosity(image, SEUIL)
            #nbcontour = ct.nb_contour(image)

        #y_gray = np.array([val[0] for val in vals])
        #y_lum = np.array([val[1] for val in vals])
        #y_nbContour = np.array([val[2] for val in vals])

        L_carac = []
        n_carac = len(vals[0][0])
        for i in range(n_carac):
            L_carac.append([np.array(crv.compl_curve([val[i] for val in vals[k]])) for k in range(N)])


        for i in range(n_carac):
            for k in range(N):
                L_carac[i][k] = L_carac[i][k]/max(L_carac[i][k])


        #pondération
        pond = np.array([1/n_carac for i in range(n_carac)])
        if len(L_carac[0]) != 1:
            for i in range(n_carac):
                pond[i] = comp.ponderation(L_carac[i])
            pond = pond/sum(pond)
            pond = -1*(pond-1)/(n_carac-1)*n_carac
        L_pond.append(pond)
        """
        #point caractéristique
        L_var_carac = [[[L_carac[i][k][n] - L_carac[i][k][n+1] for n in range(len(L_carac[0][0]))] for k in range(N)] for i in range(n_carac)]
        L_moy_var_carac = [np.mean(L_var_carac[i][k]) for k in range(N)] for i in range(n_carac)]
        L_points_carac = []
        """

    print(L_pond)










    """
        a=4
        for k in range(N):
            y_area = np.array(crv.compl_curve([val[0] for val in vals[k]]))
            y_color_R = np.array(crv.compl_curve([val[1][0] for val in vals[k]]))
            y_color_G = np.array(crv.compl_curve([val[1][1] for val in vals[k]]))
            y_color_B = np.array(crv.compl_curve([val[1][2] for val in vals[k]]))
            y_solidity = np.array(crv.compl_curve([val[2] for val in vals[k]]))
            y_phase_var = np.array(crv.compl_curve([val[3] for val in vals[k]]))

            # Normalize
            #y_gray = y_gray/max(y_gray)
            #y_lum = y_lum/max(y_lum)
            #y_nbContour = y_nbContour/max(y_nbContour)

            #y_area = y_area/max(y_area)
            #y_color_B = y_color_B/max(y_color_B)
            #y_color_R = y_color_R/max(y_color_R)
            #y_color_G = y_color_G/max(y_color_G)
            #y_solidity = y_solidity/max(y_solidity)

            # Plot
            #plt.plot(x, y_gray, label="Gray Level")
            #plt.plot(x, y_lum, label="Luminosity")
            #plt.plot(x, y_nbContour, label="Quantitée de contours")

            plt.subplot(N,a,a*k+1)
            plt.plot(x, y_area, 'o', label="Size")
            plt.ylabel("Area")
            plt.xlabel("Image rank")

            plt.subplot(N,a,a*k+2)
            plt.plot(x, y_color_B, 'bo', label="Color Blue")
            plt.plot(x, y_color_R, 'ro', label="Color Red")
            plt.plot(x, y_color_G, 'go', label="Color Green")
            plt.ylabel("Color")
            plt.xlabel("Image rank")

            plt.subplot(N,a,a*k+3)
            plt.plot(x, y_solidity,'o',label="Solidity")
            plt.ylabel("solidity")
            plt.xlabel("Image rank")

            plt.subplot(N,a,a*k+4)
            plt.plot(x, y_phase_var,'o',label="phase variation")
            plt.ylabel("phase variation")
            plt.xlabel("Image rank")

            plt.title(dir_)

            if i_dir == 0:
                plt.legend()

    plt.savefig("plot.png", format='png', dpi=100)
    plt.show()
    """
    #print("--- %s seconds ---" % (time.time() - start_time))

if __name__ == "__main__":
    main()
