# -*- coding: utf-8 -*-
"""
Created on Thu Nov 26 10:59:29 2020

Module qui permet de faire des manipulations d'images pour déterminer une valeur de luminosité

@author: Enzo Magal & Brice Berthommé
"""

import numpy as np
import mahotas as mh
"""from pylab import imshow, show"""

"""
img = mh.imread('image/image-1.jpg')                    #ouverture de mon image


T = np.array(img)       #transforme mon image en tableau de valeur
avg = np.average(T)      #calcule la moyenne des valeurs de mon tableau                           
"""

def seuillage(img, seuil):
    ### Seuille une image ###
    img = img[:, :, 0]          #transforme l'image en niveau de gris
    img2 = (img > seuil)        #seuillage
    return img2
"""
img2 = seuillage(img, 130)

imshow(img2)
show()
"""
def luminosity(nom_fichier, seuil):
    ### Calcule une valeur de luminosité ###
    image = mh.imread(nom_fichier)                      #ouverture de mon image et passage en noir et blanc
    image_seuillée = seuillage(image, seuil)
    T = np.array(image_seuillée)                        #transforme mon image en tableau de valeur
    avg = np.average(T)                                 #calcule la moyenne des valeurs de mon tableau
    return avg


